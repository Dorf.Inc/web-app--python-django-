from django.shortcuts import render, render_to_response, redirect
from lib.forms import ArtForm
from lib.models import Post
from django.views.generic import TemplateView
from django import forms
from django.views.generic.base import TemplateResponseMixin

# Create your views here.
class GenView(TemplateView):
    template_name='generator/index.html'
