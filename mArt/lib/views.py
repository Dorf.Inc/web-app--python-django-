from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from lib.models import Post
from django.shortcuts import render, render_to_response, redirect
from lib.forms import ArtForm

from django.views.generic import TemplateView
from django import forms
from django.views.generic.base import TemplateResponseMixin
from django.core.files.storage import FileSystemStorage

# Create your views here.
class LibView(TemplateView):
    template_name='lib/lib.html'

    # def get(self,request):
    #     posts=Post.objects.all()
    #     print(posts)
    #     return render(request,self.template_name,{'posts':posts})

    def galery(request):
        imgs=Post.objects.all()
        return render(request,'lib/lib.html',{'imgs':imgs})


    # Create your views here.

    # def get(self,request):
    #         form=ArtForm()
    #         #form.fields['post'].widget = forms.HiddenInput()
    #         form.fields['post'].required = False
    #         posts=Post.objects.all()
    #         print(posts)
    #         return render(request,self.template_name,{'form':form})
    #
    def upload(request):
        if request.method=='POST':
            form=ArtForm(request.POST,request.FILES);
            if form.is_valid():
                form.save()
                return redirect('galery')
        else:
            form=ArtForm()

        return render(request,'lib/upload.html',{'form':form})
