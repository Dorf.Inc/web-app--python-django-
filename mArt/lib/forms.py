from django import forms
from lib.models import Post

class ArtForm(forms.ModelForm):
    post=forms.ImageField()

    class Meta:
        model = Post
        fields=("post",)
