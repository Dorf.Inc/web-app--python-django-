var amount=3;
var isRandomBg=false;
var fArray=[];

function makeArt(){
  getSettings();
    $("#holst").css('background-color', getRandomColor());



  clearCanvas();
  var i;
  fArray.push($("#holst").css('background-color'));
  for (i = 0; i < amount; i++) {
    placeFigure();
  }
  console.log(amount);
  sGalery();
}
function clearCanvas(){
  fArray=[];
  canvas = document.getElementById('hcnv');
  ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function placeFigure() {
  var imgFrame = new Image();
  imgFrame.src = 'static/figures/'+Math.round(getRandomArbitrary(1,5))+'/'+Math.round(getRandomArbitrary(1,5))+'.png';
  canvas = document.getElementById('hcnv');
  var ctx = canvas.getContext('2d');
  //ctx.save();
  var size=getRandomArbitrary(10,500);

  //ctx.rotate(getRandomArbitrary(0,360)*Math.PI/180);
  var ranX=getRandomArbitrary(-size/2,700-size/2);
  var ranY=getRandomArbitrary(-size/2,700-size/2);
  ctx.drawImage(imgFrame,ranX,ranY,size,size);
  //ctx.drawImage(imgFrame,600,600,10,10);

  //ctx.restore();
  fArray.push(imgFrame.src.substring(29));
  fArray.push(ranX);
  fArray.push(ranY);
  fArray.push(size);

}
function sGalery(){
  var string="";
  for(i=0;i<fArray.length;i++){
    string=string+fArray[i]+"&";
  }
  document.getElementById("id_post").value=string;
  console.log(  document.getElementById("id_post").value);
}
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
function getRandomColor()
{
  var letters = '0123456789ABCDEF'.split('');
  var color = '#';
  for (var i = 0; i < 6; i++ )
  {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function getSettings(){
  if(  $("#amountOfFigures").val()!=""){
      window.amount=$("#amountOfFigures").val();
  }
  isRandomBg=$("#randomizeBg").val();
}

download_img= function(el) {
  canvas = document.getElementById('hcnv');
  var ctx = canvas.getContext('2d');
  ctx.globalCompositeOperation = 'destination-over';
  ctx.fillStyle = $("#holst").css('background-color');
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  var imageURI = canvas.toDataURL("image/jpg");
  el.href = imageURI;
};
//

//});
