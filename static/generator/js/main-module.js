var amount=3

function makeArt(){
  clearCanvas();
  var i;
  for (i = 0; i < amount; i++) {
    placeFigure();
  }
}
function clearCanvas(){
  canvas = document.getElementById('hcnv');
  ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function placeFigure() {
  var imgFrame = new Image();
  imgFrame.src = 'figures/circle-ring-gray.png';
  canvas = document.getElementById('hcnv');
  var ctx = canvas.getContext('2d');
  var size=getRandomArbitrary(10,400);


  ctx.drawImage(imgFrame,getRandomArbitrary(0,700),getRandomArbitrary(0,700),size,size);
}
function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
function getRandomColor()
{
  var letters = '0123456789ABCDEF'.split('');
  var color = '#';
  for (var i = 0; i < 6; i++ )
  {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
function setSettings(){
  var data = $('#setform').serializeArray().reduce(function(obj, item) {
    obj[item.name] = item.value;
    return obj;
    }, {});
  //amount=
  Console.log()
}
